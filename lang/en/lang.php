<?php return [
    'plugin' => [
        'name' => 'Restaurant',
        'description' => '',
    ],
    'name' => 'Name',
    'image' => 'Image',
    'slug' => 'Slug',
    'ref_code' => 'Reference Code #',
    'ref_code_placeholder' => 'Ex: 0001, ABC123, 001-2345, ...',
    'description' => 'Description',
    'categories' => 'Categories',
    'price' => 'Price',
    'updated_at' => 'Latest Update',
    'food_menu' => 'Food',
    'set_menu_menu' => 'Set Menu',
    'category_menu' => 'Category',
];