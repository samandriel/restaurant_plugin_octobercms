<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestaurantCategories4 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restaurant_categories', function($table)
        {
            $table->integer('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restaurant_categories', function($table)
        {
            $table->integer('id')->unsigned()->change();
        });
    }
}
