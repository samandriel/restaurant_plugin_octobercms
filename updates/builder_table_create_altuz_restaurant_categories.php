<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestaurantCategories extends Migration
{
    public function up()
    {
        Schema::create('altuz_restaurant_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id')->unsigned();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('ref_code')->nullable();
            $table->string('slug');
            $table->string('image_url')->nullable();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restaurant_categories');
    }
}
