<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestaurantFoodsSetMenus extends Migration
{
    public function up()
    {
        Schema::create('altuz_restaurant_foods_set_menus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('set_menu_id')->unsigned();
            $table->integer('food_id');
            $table->primary(['set_menu_id','food_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restaurant_foods_set_menus');
    }
}
