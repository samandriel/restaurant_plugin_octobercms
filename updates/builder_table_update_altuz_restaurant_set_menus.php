<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestaurantSetMenus extends Migration
{
    public function up()
    {
        Schema::table('altuz_restaurant_set_menus', function($table)
        {
            $table->decimal('price', 10, 0);
            $table->string('image')->nullable(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restaurant_set_menus', function($table)
        {
            $table->dropColumn('price');
            $table->string('image', 255)->nullable()->change();
        });
    }
}
