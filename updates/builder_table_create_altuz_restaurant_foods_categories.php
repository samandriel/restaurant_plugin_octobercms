<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestaurantFoodsCategories extends Migration
{
    public function up()
    {
        Schema::create('altuz_restaurant_foods_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('category_id')->unsigned();
            $table->integer('food_id')->unsigned();
            $table->primary(['category_id','food_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restaurant_foods_categories');
    }
}
