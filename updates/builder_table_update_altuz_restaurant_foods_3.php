<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAltuzRestaurantFoods3 extends Migration
{
    public function up()
    {
        Schema::table('altuz_restaurant_foods', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('altuz_restaurant_foods', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
