<?php namespace Altuz\Restaurant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAltuzRestaurantFoods extends Migration
{
    public function up()
    {
        Schema::create('altuz_restaurant_foods', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('ref_code')->nullable();
            $table->string('slug');
            $table->string('image')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('altuz_restaurant_foods');
    }
}
